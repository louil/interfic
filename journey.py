#!/usr/bin/env/python3

import journey_classes as j
import time
import os

#Function that gets user input
def get_choice():
    choice = input("What would you like to do: ")
    return choice.lower()

#Function that clears the screen after a 3 second delay
def clear_screen():
    time.sleep(3)
    os.system("clear")

#Function that prints out the default death
def not_making_choice():
    print("You have died of dysentery.")
    clear_screen()

#Function that gives the initial output upon starting the program
def starting_location():
    print("You have awaken to find yourself in a new location, almost like a new world...")
    print("Its very bright, the sun is high in the air giving you the feeling that it is mid day.")
    print("You look around and take account of your surroundings.")
    print("Right next to you is a prisim and reusty helmet conveniently placed.")
    print("To the north is a door connected to a giant creepy lookin mansion.")
    print("To the east is a cliff, the south leads to a vast grassy plain.")
    print("Finally to the west is another cliff, it seems the mansion is located at the top.")
    print("You get a feeling that grabbing the prism and hemet then going through the door would be your best option.")

#Function that makes checks and gives output for the starting zone
def location_zero(sceen, choice, answer, step):
    if sceen.start(choice) == 0 and step == 1:
        answer.append(choice)
        print("You grab both the prism and helmet, and with but a thought store them away.")
        clear_screen()
        print("Afterwards you think about opening the door to the mansion.")
        return 1
    elif sceen.start(choice) == 1 and step == 1:
        answer.append("get all")
        print("You grab both the prism and helmet, and with but a thought store them away.")
        clear_screen()
        print("Afterwards you think about opening the door to the mansion.")
        return 1
    elif sceen.start(choice) == 2 and step == 2:
        answer.append(choice)
        print("You decide to enter the spooky scary mansion and open the door entering inside after creaking to door open.")
        clear_screen()
        print("Upon entering the mansion you take a quick glance at your surroundings, noticing that the mansion is ancient.")
        return 2
    else:
        print("A spector appears killing you instantly.")
        clear_screen()
        return 3

#Function that makes checks and gives output for the next zone
def location_one(sceen, choice, answer, step):
    if sceen.first_room(choice) == 1 and step == 3:
        answer.append(choice)
        print("You go to the eastern part of the mansion.")
        clear_screen()
        print("Upon entering the room you see stairs that go up next to a hallway with pictures that seem to stare at you no matter the angle you look at them.")
        print("You are drawn to one of the pictures that has an edelweiss on it.")
        return 1
    else:
        print("You feel as if a divine entity has told you to get the edelweiss, unfortunately it smote you where you stood.")
        clear_screen()
        return 2

#Function that makes checks and gives output for the next zone
def location_two(sceen, choice, answer, step):
    if sceen.east_room(choice) == 0 and step == 4:
        answer.append(choice)
        print("You picked up the edelweiss and like the prism and helmet before store it away with but a thought.")
        clear_screen()
        print("Now that you have the edelweiss the disire to explore upstairs intensifies.")
        return 1
    elif sceen.east_room(choice) == 1 and step == 4:
        answer.append("get edelweiss")
        print("You picked up the edelweiss and like the prism and helmet before store it away with but a thought.")
        clear_screen()
        print("Now that you have the edelweiss the disire to explore upstairs intensifies.")
        return 1
    elif sceen.east_room(choice) == 2 and step == 5:
        answer.append(choice)
        print("You head upstairs.")
        clear_screen()
        print("After climbing those stairs you pant heavily wishing that you had exercised more.")
        print("The room you arrived in is messy with a thick layer of dust on everything, you notice a huge wardrobe missing it's doors with a tunnle that seems to slope down, like a cave.")
        return 2
    else:
        print("You instantly regret your decision and take it back but can't as you fall through the eroded floor and upon hitting the lowest floor lose conciousness.")
        clear_screen()
        return 3

#Function that makes checks and gives output for the next zone
def location_three(sceen, choice, answer, step):
    if sceen.second_floor(choice) == 1 and step == 6:
        answer.append(choice)
        print("You enter the cave.")
        clear_screen()
        print("Upon entering the cave after stumbling around for a while you trip and fall into a reasonably large firepit. You get up out of the pit and brush yourself off.")
        return 1
    else:
        print("Everything fades to black.")
        clear_screen()
        return 2

#Function that makes checks and gives output for the next zone
def location_four(sceen, choice, answer, step):
    if sceen.cave(choice) == 0 and step == 7:
        answer.append(choice)
        print("You think if only this firepit would be lit, and of course it spontaneously bursts... however your exitement is short lived as it's a tiny fire.")
        clear_screen()
        print("As the fire is small you can't really make anything of your surroundings.")
        return 1
    elif sceen.cave(choice) == 1 and step == 8:
        answer.append(choice)
        print("You decide to wait for the fire to get larger.")
        clear_screen()
        print("After waiting for a bit parts of the fire seem to seperate and float into the air, stopping in front of you and forming characters that you seem to be able to read.")
        print("Without knowing how or why the characters seem to say put the edelweiss in the fire or else.")
        return 2
    elif sceen.cave(choice) == 2 and step == 9:
        answer.append(choice)
        print("Fearing any repercussions you quickly put the edelweiss into the fire.")
        clear_screen()
        print("As soon as the edelweiss touches the flames a loud shrill noise sounds out.")
        print("After a little the fire rises up to the ceiling of the cave condensing into the form of a ancient warrior holding a bowl containg gear for battle, however you notice there is no helmet.")
        return 3
    elif sceen.cave(choice) == 3 and step == 10:
        answer.append(choice)
        print("You remember that helmet from before and place in the bowl. Suddenly the statue glows bright and a brilliant light expands out blinding you.")
        print("After your vision returns you see that the bowl is gone and the statue is fully outfitted, including the helmet you placed earlier.")
        clear_screen()
        print("Looking at the warrior statue you see what shockingly looks like a pickle in the place where the heart would be.")
        print("The pickle is relatively large and has a empty space in the middle, which oddly enough seems to be in the shape of a prism.")
        return 4
    elif sceen.cave(choice) == 4 and step == 11:
        answer.append(choice)
        print("You put the prism in the pickle and retreat quickly to avoid whatever could possibly happen next.")
        print("The pickle seems to become one as the prism dissapears inside of it. Following that the statue absorbs the pickle.")
        clear_screen()
        print("The statue seems to come alive and as you stand there you here a voice ask you if you are French.")
        print("Before you can answer the lifelike statue farts in you direction and runs off.")
        return 5
    elif sceen.cave(choice) == 5 and step == 12:
        answer.append(choice)
        print("You exit the cave.")
        clear_screen()
        print("A bright light assaults your eyes, and after adjusting you notice that you are standing on a beach with the water to your north.")
        return 6
    else:
        print("You feel as though something isn't right as you hear a loud rumbling sound. You look up just in time to see a giant pice of the ceiling towards your face.")
        clear_screen()
        return 7

#Function that makes checks and gives output for the next zone
def location_five(sceen, choice, answer, step):
    if sceen.beach(choice) == 0 and step == 13:
        answer.append(choice)
        print("You decide to walk out into the water, only to discover that you aren't sinking, you are actually walking on water!")
        clear_screen()
        print("Now you stand shocked on water, like your the second coming of christ or something.")
        return 1
    elif sceen.beach(choice) == 1 and step == 14:
        answer.append(choice)
        print("You get the meaning of life.")
        clear_screen()
        return 2
    else:
        print("Giant tentacles erupt from the water quickly wrapping around you. Quickly you are dragged into the depths as you look down in fear to see a giant sea creature opening it's mouth.")
        clear_screen()
        return 3

def main():
    correct_solution = ["get all", "open door", "east", "get edelweiss", "up", "enter cave", "light fire", "wait", "put edelweiss in fire", \
                        "put helmet in statue", "put prism in pickle", "exit cave", "north", "get meaning of life"]
    verbs = ["get all", "take all", "open door", "east", "get edelweiss", "take edelweiss", "up", "enter cave", "light fire", "wait", "put edelweiss in fire", \
                        "put helmet in statue", "put prism in pickle", "exit cave", "north", "get meaning of life"]
    location = 0
    answer = []
    did_you_die = 0
    step = 1
    sceen = j.Sceen()

    #Main loop that goes through and makes sure that the player eventually gets to the end, otherwise killing them and making them start over
    while True:
        if did_you_die == 1:
            step = 1
            did_you_die = 0
            del answer[:]
            location = 0
            print("WOOOSSSSHHH!!!!")
            print("Your conciousness returns and upon looking around you seem to appear where you first woke up.")
            print("You get an ominous feeling as everything looks the same as before, leading you to wonder what you have gotten yourself into.")
        else:
            starting_location()

        while (location <= 5):
            if (location == 0):
                choice = get_choice()
                if choice in verbs:
                    result = location_zero(sceen, choice, answer, step)
                    if result == 1:
                        step += 1
                    elif result == 2:
                        location += 1
                        step += 1
                    elif result == 3:
                        did_you_die = 1
                        break
                else:
                    not_making_choice()
                    did_you_die = 1
                    break
            elif (location == 1):
                choice = get_choice()
                if choice in verbs:
                    result = location_one(sceen, choice, answer, step)
                    if result == 1:
                        step += 1
                        location += 1
                    elif result == 2:
                        did_you_die = 1
                        break
                else:
                    not_making_choice()
                    did_you_die = 1
                    break
            elif (location == 2):
                choice = get_choice()
                if choice in verbs:
                    result = location_two(sceen, choice, answer, step)
                    if result == 1:
                        step += 1
                    elif result == 2:
                        location += 1
                        step += 1
                    elif result == 3:
                        did_you_die = 1
                        break
                else:
                    not_making_choice()
                    did_you_die = 1
                    break
            elif (location == 3):
                choice = get_choice()
                if choice in verbs:
                    result = location_three(sceen, choice, answer, step)
                    if result == 1:
                        location += 1
                        step += 1
                    elif result == 2:
                        did_you_die = 1
                        break
                else:
                    not_making_choice()
                    did_you_die = 1
                    break
            elif (location == 4):
                choice = get_choice()
                if choice in verbs:
                    result = location_four(sceen, choice, answer, step)
                    if result > 0 and result < 6:
                        step += 1
                    elif result == 6:
                        step += 1
                        location += 1
                    elif result == 7:
                        did_you_die = 1
                        break
                else:
                    not_making_choice()
                    did_you_die = 1
                    break
            elif (location == 5):
                choice = get_choice()
                if choice in verbs:
                    result = location_five(sceen, choice, answer, step)
                    if result == 1:
                        step += 1
                    elif result == 2:
                        step += 1
                        location += 1
                    elif result == 3:
                        did_you_die = 1
                        break
                else:
                    not_making_choice()
                    did_you_die = 1
                    break

        if set(answer) == set(correct_solution):
            print("A loud DING noise is heard as everything goes black.")
            print("Your eyes open and you recognize your surroundings, you are back in your room.")
            print("You assume that it was all just a dream, little did you know that this was but the beginning of something that you could never imagine.")
            exit()
        else:
            did_you_die = 1

if __name__ == "__main__":
    main()
