#!/usr/bin/env/python3

class Sceen:
    def __init__(self):
        self._accepted = []
        
    def start(self, choice):
        self._accepted = ["get all", "take all", "open door"]
        if choice in self._accepted:
              return self._accepted.index(choice)
        else:
            return 3
            
    def first_room(self, choice):
        self._accepted = ["east"]
        if choice in self._accepted:
            return 1
        else:
            return 2

    def east_room(self, choice):
        self._accepted = ["get edelweiss", "take edelweiss", "up"]
        if choice in self._accepted:
            return self._accepted.index(choice)
        else:
            return 3
      
    def second_floor(self, choice):
        self._accepted = ["enter cave"]
        if choice in self._accepted:
            return 1
        else:
            return 2
            
    def cave(self, choice):
        self._accepted = ["light fire", "wait", "put edelweiss", "put helmet in statue", "put prism in pickle", "exit cave"]
        if choice in self._accepted:
            return self._accepted.index(choice)
        else:
            return 2
            
    def beach(self, choice):
        self._accepted = ["north", "get meaning of life"]
        if choice in self._accepted:
            return self._accepted.index(choice)
        else:
            return 2

        
        